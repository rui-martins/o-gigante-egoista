using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DefaultExecutionOrder(2)]
public class SpriteEraserController : MonoBehaviour
{
    [SerializeField] private LayerMask eraserMask;

    private bool isTouching;
    private Vector2 touchScreenPos;
    private Vector2 touchViewportPos;
    private Material mat;
    private List<Vector4> posList = new List<Vector4>(2);

    private void Awake()
    {
        mat = GetComponent<Renderer>().material;
        posList.Add(new Vector2(0.5f, 0.5f));
        posList.Add(new Vector2(0.5f, 0.9f));
    }

    private void OnEnable()
    {
        Managers.InputManager.OnStartTouch += StartTouch;
        Managers.InputManager.OnEndTouch += EndTouch;
    }

    private void OnDisable()
    {
        Managers.InputManager.OnStartTouch -= StartTouch;
        Managers.InputManager.OnEndTouch -= EndTouch;
    }

    private void StartTouch(Vector2 pos, float time)
    {
        isTouching = true;
    }

    private void EndTouch(Vector2 pos, float time)
    {
        posList.Clear();
        isTouching = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (isTouching)
        {
            touchScreenPos = Managers.InputManager.GetTouchPosition(PositionType.Screen);
            touchViewportPos = Managers.MainCam.ScreenToViewportPoint(touchScreenPos);


            //posList.Add(touchViewportPos);

            Ray touchRay = Managers.MainCam.ScreenPointToRay(touchScreenPos);
            RaycastHit2D[] hits = Physics2D.GetRayIntersectionAll(touchRay, 500f, eraserMask);

            foreach (RaycastHit2D hit in hits)
            {
                mat.SetVector("_CutoutPos", touchViewportPos);
            }
        }
    }
}
