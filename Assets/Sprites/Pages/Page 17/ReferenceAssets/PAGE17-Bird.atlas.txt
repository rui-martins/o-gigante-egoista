
PAGE17-Bird.png
size: 4096,512
format: RGBA8888
filter: Linear,Linear
repeat: none
images/bird/BICO-FECHADO
  rotate: false
  xy: 1123, 28
  size: 139, 42
  orig: 139, 42
  offset: 0, 0
  index: -1
images/bird/NOTA
  rotate: true
  xy: 2678, 11
  size: 101, 121
  orig: 101, 121
  offset: 0, 0
  index: -1
images/bird/OLHOS
  rotate: true
  xy: 861, 22
  size: 48, 44
  orig: 48, 44
  offset: 0, 0
  index: -1
images/bird/OLHOS_TRAS
  rotate: false
  xy: 1789, 12
  size: 119, 85
  orig: 119, 85
  offset: 0, 0
  index: -1
images/bird/asa-static
  rotate: false
  xy: 3377, 224
  size: 251, 286
  orig: 251, 286
  offset: 0, 0
  index: -1
images/bird/bico-aberto-baixo
  rotate: false
  xy: 738, 11
  size: 121, 59
  orig: 121, 59
  offset: 0, 0
  index: -1
images/bird/bico-aberto-cima
  rotate: false
  xy: 1264, 2
  size: 113, 75
  orig: 113, 75
  offset: 0, 0
  index: -1
images/bird/body
  rotate: true
  xy: 1789, 99
  size: 411, 545
  orig: 411, 545
  offset: 0, 0
  index: -1
images/bird/cauda
  rotate: false
  xy: 1130, 79
  size: 307, 431
  orig: 307, 431
  offset: 0, 0
  index: -1
images/bird/head
  rotate: true
  xy: 3630, 8
  size: 266, 252
  orig: 266, 252
  offset: 0, 0
  index: -1
images/bird/lingua
  rotate: false
  xy: 2, 7
  size: 55, 23
  orig: 55, 23
  offset: 0, 0
  index: -1
images/bird/olho-mancha
  rotate: false
  xy: 1379, 6
  size: 105, 71
  orig: 105, 71
  offset: 0, 0
  index: -1
images/bird/pata-frente-P1
  rotate: true
  xy: 1910, 13
  size: 84, 97
  orig: 84, 97
  offset: 0, 0
  index: -1
images/bird/pata-frente-P2
  rotate: true
  xy: 2336, 3
  size: 109, 169
  orig: 109, 169
  offset: 0, 0
  index: -1
images/bird/pata-frente-P3
  rotate: false
  xy: 59, 8
  size: 67, 22
  orig: 67, 22
  offset: 0, 0
  index: -1
images/bird/pata-frente-P4
  rotate: false
  xy: 197, 9
  size: 81, 21
  orig: 81, 21
  offset: 0, 0
  index: -1
images/bird/pata-frente-P5
  rotate: false
  xy: 1486, 42
  size: 114, 37
  orig: 114, 37
  offset: 0, 0
  index: -1
images/bird/pata-frente-P6
  rotate: false
  xy: 907, 23
  size: 106, 47
  orig: 106, 47
  offset: 0, 0
  index: -1
images/bird/pata-tras-P1
  rotate: true
  xy: 2009, 13
  size: 84, 97
  orig: 84, 97
  offset: 0, 0
  index: -1
images/bird/pata-tras-P2
  rotate: true
  xy: 2507, 3
  size: 109, 169
  orig: 109, 169
  offset: 0, 0
  index: -1
images/bird/pata-tras-P3
  rotate: false
  xy: 128, 8
  size: 67, 22
  orig: 67, 22
  offset: 0, 0
  index: -1
images/bird/pata-tras-P4
  rotate: false
  xy: 280, 17
  size: 81, 21
  orig: 81, 21
  offset: 0, 0
  index: -1
images/bird/pata-tras-P5
  rotate: false
  xy: 1486, 3
  size: 114, 37
  orig: 114, 37
  offset: 0, 0
  index: -1
images/bird/pata-tras-P6
  rotate: false
  xy: 1015, 23
  size: 106, 47
  orig: 106, 47
  offset: 0, 0
  index: -1
images/bird/wing-A
  rotate: true
  xy: 3630, 276
  size: 234, 271
  orig: 234, 271
  offset: 0, 0
  index: -1
images/bird/wing-B
  rotate: false
  xy: 3223, 28
  size: 137, 167
  orig: 137, 167
  offset: 0, 0
  index: -1
images/bird/wing-C
  rotate: false
  xy: 738, 72
  size: 390, 438
  orig: 390, 438
  offset: 0, 0
  index: -1
images/bird/wing-C2
  rotate: true
  xy: 208, 40
  size: 470, 528
  orig: 470, 528
  offset: 0, 0
  index: -1
images/bird/wing-D
  rotate: true
  xy: 1439, 81
  size: 429, 348
  orig: 429, 348
  offset: 0, 0
  index: -1
images/bird/wing-closed-A
  rotate: true
  xy: 3223, 197
  size: 313, 152
  orig: 313, 152
  offset: 0, 0
  index: -1
images/bird/wing-closed-B
  rotate: false
  xy: 2801, 12
  size: 250, 115
  orig: 250, 115
  offset: 0, 0
  index: -1
images/bird/wing-closed-C
  rotate: true
  xy: 2336, 114
  size: 396, 364
  orig: 396, 364
  offset: 0, 0
  index: -1
images/bird/wing-closed-C2
  rotate: false
  xy: 2702, 129
  size: 519, 381
  orig: 519, 381
  offset: 0, 0
  index: -1
images/bird/wing-closed-D
  rotate: true
  xy: 2, 32
  size: 478, 204
  orig: 478, 204
  offset: 0, 0
  index: -1
