
PAGE20_Flying_Birds.png
size: 512,256
format: RGBA8888
filter: Linear,Linear
repeat: none
images/asa-fechada
  rotate: false
  xy: 2, 109
  size: 94, 141
  orig: 94, 141
  offset: 0, 0
  index: -1
images/asa-fechar
  rotate: false
  xy: 188, 7
  size: 92, 101
  orig: 92, 101
  offset: 0, 0
  index: -1
images/asa_frente
  rotate: false
  xy: 98, 110
  size: 89, 140
  orig: 89, 140
  offset: 0, 0
  index: -1
images/asa_tras
  rotate: true
  xy: 189, 120
  size: 130, 74
  orig: 130, 74
  offset: 0, 0
  index: -1
images/corpo
  rotate: true
  xy: 2, 2
  size: 105, 91
  orig: 105, 91
  offset: 0, 0
  index: -1
images/corpo-azul
  rotate: true
  xy: 95, 2
  size: 105, 91
  orig: 105, 91
  offset: 0, 0
  index: -1
images/leg_back
  rotate: false
  xy: 265, 143
  size: 19, 38
  orig: 19, 38
  offset: 0, 0
  index: -1
images/leg_back_2
  rotate: true
  xy: 282, 83
  size: 26, 11
  orig: 26, 11
  offset: 0, 0
  index: -1
images/leg_back_3
  rotate: false
  xy: 299, 160
  size: 20, 21
  orig: 20, 21
  offset: 0, 0
  index: -1
images/leg_front
  rotate: false
  xy: 265, 111
  size: 24, 30
  orig: 24, 30
  offset: 0, 0
  index: -1
images/leg_front_2
  rotate: true
  xy: 286, 153
  size: 28, 11
  orig: 28, 11
  offset: 0, 0
  index: -1
images/leg_front_3
  rotate: true
  xy: 291, 127
  size: 24, 20
  orig: 24, 20
  offset: 0, 0
  index: -1
images/rabo
  rotate: true
  xy: 265, 183
  size: 67, 46
  orig: 67, 46
  offset: 0, 0
  index: -1
