using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SwipeDetection : MonoBehaviour
{
    [SerializeField] private GameObject trail;
    [SerializeField] private float minDistance = 1f;
    [SerializeField] private float maxTime = 1f;
    [SerializeField][Range(0, 1)] private float dirThreshold = 0.9f;

    public Action OnSwipeUp;
    public Action OnSwipeRight;

    private InputManager inputManager;
    private Vector2 startPosition;
    private float startTime;
    private Vector2 endPosition;
    private float endTime;
    private Coroutine trailRoutine;

    private void Awake()
    {
        inputManager = InputManager.Instance;
    }

    private void OnEnable()
    {
        inputManager.OnStartTouch += SwipeStart;
        inputManager.OnEndTouch += SwipeEnd;
    }

    private void OnDisable()
    {
        inputManager.OnStartTouch -= SwipeStart;
        inputManager.OnEndTouch -= SwipeEnd;
    }

    private void SwipeStart(Vector2 position, float time)
    {
        startPosition = position;
        startTime = time;
        trail.SetActive(true);
        trail.transform.position = position;
        trailRoutine = StartCoroutine(Trail());
    }

    private IEnumerator Trail()
    {
        while (true)
        {
            trail.transform.position = inputManager.GetTouchPosition(PositionType.World);
            yield return null;
        }
    }

    private void SwipeEnd(Vector2 position, float time)
    {
        StopCoroutine(trailRoutine);
        trail.SetActive(false);
        endPosition = position;
        endTime = time;

        DetectSwipe();
    }

    private void DetectSwipe()
    {
        if (Vector3.Distance(startPosition, endPosition) >= minDistance &&
            (endTime - startTime) <= maxTime)
        {
            Debug.DrawLine(startPosition, endPosition, Color.red, 5f);
            Vector3 direction = (endPosition - startPosition).normalized;
            GetSwipeDirection(direction);
        }
    }

    private void GetSwipeDirection(Vector2 direction)
    {
        if (Vector2.Dot(Vector2.up, direction) > dirThreshold)
            OnSwipeUp?.Invoke();
        if (Vector2.Dot(Vector2.down, direction) > dirThreshold)
            Debug.Log("Swipe Down");
        if (Vector2.Dot(Vector2.right, direction) > dirThreshold)
            OnSwipeRight?.Invoke();
        if (Vector2.Dot(Vector2.left, direction) > dirThreshold)
            print("Swipe Left");
    }
}
