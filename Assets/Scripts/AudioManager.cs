using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;

[DefaultExecutionOrder(-2)]
public class AudioManager : MonoBehaviour
{
    [SerializeField] private AudioSource voiceOverAS;
    [SerializeField] private AudioSource soundFxAS;
    [SerializeField] private AudioSource ambientAS;
    [SerializeField] private AudioSource secAmbientAS;

    private static AudioManager instance;
    public static AudioManager Instance => instance;

    public AudioSource VoicerOverAS => voiceOverAS;
    public AudioSource SoundFxAS => soundFxAS;
    public AudioSource AmbientAS => ambientAS;
    public AudioSource AmbientAS2 => secAmbientAS;

    private void Awake()
    {
        instance = this;
    }

    public void Play(AudioSourceType audioSourceType, AudioClip clip, float startTime = 0, float delay = 0)
    {
        switch (audioSourceType)
        {
            case AudioSourceType.VoiceOver:
                voiceOverAS.clip = clip;
                voiceOverAS.Play();
                break;
            case AudioSourceType.SoundFX:
                soundFxAS.clip = clip;
                soundFxAS.Play();
                break;
            case AudioSourceType.Ambient:
                if (ambientAS.isPlaying)
                {
                    SwitchAmbientAS(secAmbientAS, ambientAS, clip, 2f, startTime, delay);
                }
                else if (secAmbientAS.isPlaying)
                {
                    SwitchAmbientAS(ambientAS, secAmbientAS, clip, 2f, startTime, delay);
                }
                else
                {
                    ambientAS.clip = clip;
                    if (startTime > 0) ambientAS.time = startTime;
                    if (delay > 0) ambientAS.PlayDelayed(delay);
                    else ambientAS.Play();
                }
                break;
        }
    }

    private async void SwitchAmbientAS(AudioSource fadeInSource, AudioSource fadeOutSource, AudioClip clip, float duration, float startTime = 0, float delay = 0)
    {
        float currentTime = 0;
        float fadeInSourceVol = fadeInSource.volume;
        float fadeOutSourceVol = fadeOutSource.volume;
        float fadeInSourceTarget = 1;
        float fadeOutSourceTarget = 0;
        fadeInSource.clip = clip;

        if (startTime > 0) fadeInSource.time = startTime;

        if (delay > 0) fadeInSource.PlayDelayed(delay);
        else fadeInSource.Play();

        while (currentTime < duration)
        {
            currentTime += Time.deltaTime;

            fadeInSource.volume = Mathf.Lerp(fadeInSourceVol, fadeInSourceTarget, currentTime / duration);
            fadeOutSource.volume = Mathf.Lerp(fadeOutSourceVol, fadeOutSourceTarget, currentTime / duration);
            await Task.Yield();
        }

        fadeOutSource.Stop();
    }

    public enum AudioSourceType
    {
        VoiceOver,
        SoundFX,
        Ambient
    }
}
