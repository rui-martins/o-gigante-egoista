using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.Linq;
using System;

[DefaultExecutionOrder(2)]
public class PageSwitchManager : MonoBehaviour
{
    [Header("Settings for Jo�o")]
    [SerializeField] private bool deactivateHiddenPagesOnStart;
    [SerializeField] private bool deactivateHiddenPageAfterChanging;

    [Header("General Settings")]
    [SerializeField] private Transform pagesHolder;
    [SerializeField] private float pageSlideTime;
    [SerializeField] private AnimationCurve pageSwitchCurve;

    private List<Page> pages = new List<Page>();
    private Page currentPage;
    private float xPageNextCoords;
    private bool isChangingPage;

    private struct Page
    {
        public int PageNum { get; private set; }
        public Transform PageObj { get; private set; }

        public Page(int pageNum, Transform pageObj)
        {
            PageNum = pageNum;
            PageObj = pageObj;
        }
    }

    private void Start()
    {
        int i = 0;

        List<Page> pagesInversed = new List<Page>();

        foreach (Transform t in pagesHolder)
        {
            pagesInversed.Add(new Page(i++, t));
        }

        i = 0;

        for (int j = pagesInversed.Count - 1; j >= 0; j--)
        {
            pages.Add(new Page(i++, pagesInversed[j].PageObj));
        }

        currentPage = pages[0];

        if (deactivateHiddenPagesOnStart)
        {
            foreach (Page p in pages)
            {
                if (p.PageNum != currentPage.PageNum)
                    p.PageObj.gameObject.SetActive(false);
            }
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow) && !isChangingPage)
        {
            SlidePage();
        }
    }

    public void SlidePage(float delay = 0f)
    {
        if (currentPage.PageNum == pages.Count - 1) return;

        xPageNextCoords = -pages[currentPage.PageNum + 1].PageObj.position.x;

        // SAVE //
        SaveMenuManager.Instance.SetCurrentPage(currentPage.PageNum + 1);
        isChangingPage = true;

        Sequence pageTurnSeq = DOTween.Sequence();
        Transform nextPage = pages[currentPage.PageNum + 1].PageObj;
        PageSettings pageSettings = nextPage.GetComponent<PageSettings>();
        float slideTime = 0f;

        if (pageSettings != null)
        {
            slideTime = pageSettings.TransitionSpeed;
            xPageNextCoords = -pageSettings.PageDistance;
            nextPage.position = new Vector3(pageSettings.PageDistance, nextPage.position.y, nextPage.position.z);
        }
        else
        {
            slideTime = pageSlideTime;
        }

        pageTurnSeq
        .SetDelay(delay)
        .AppendCallback(() => nextPage.gameObject.SetActive(true))
        .Append(pages[currentPage.PageNum].PageObj.DOLocalMoveX(xPageNextCoords, slideTime))
        .Join(nextPage.DOLocalMoveX(0, slideTime))
        .SetEase(Ease.InOutSine)
        .AppendCallback(() => FireNextPageEvents(pageSettings))
        .AppendInterval(slideTime + 0.1f)
        .AppendCallback(() => SwitchActivePage(currentPage.PageNum + 1));
    }

    public void SlidePageFromSaveFile(int startPageNum)
    {
        if (currentPage.PageNum == pages.Count - 1) return;

        print(pages.Count);

        pages[0].PageObj.gameObject.SetActive(false);
        currentPage = pages[pages.Count - 1];
        currentPage.PageObj.gameObject.SetActive(true);

        xPageNextCoords = -pages[startPageNum].PageObj.position.x;

        // SAVE //
        SaveMenuManager.Instance.SetCurrentPage(startPageNum);
        isChangingPage = true;

        Sequence pageTurnSeq = DOTween.Sequence();
        Transform nextPage = pages[startPageNum].PageObj;
        PageSettings pageSettings = nextPage.GetComponent<PageSettings>();
        float slideTime = 0f;

        print(startPageNum);

        if (pageSettings != null)
        {
            slideTime = pageSettings.TransitionSpeed;
            xPageNextCoords = -pageSettings.PageDistance;
            nextPage.position = new Vector3(pageSettings.PageDistance, nextPage.position.y, nextPage.position.z);
        }
        else
        {
            slideTime = pageSlideTime;
        }

        pageTurnSeq
        .AppendCallback(() => nextPage.gameObject.SetActive(true))
        .Append(pages[currentPage.PageNum].PageObj.DOLocalMoveX(xPageNextCoords, slideTime))
        .Join(nextPage.DOLocalMoveX(0, slideTime))
        .SetEase(Ease.InOutSine)
        .AppendCallback(() => FireNextPageEvents(pageSettings))
        .AppendInterval(slideTime + 0.1f)
        .AppendCallback(() => SwitchActivePage(startPageNum));
    }

    private void SwitchActivePage(int nextPageNum)
    {
        if (deactivateHiddenPageAfterChanging)
            currentPage.PageObj.gameObject.SetActive(false);

        currentPage = pages[nextPageNum];

        isChangingPage = false;
    }

    private void FireNextPageEvents(PageSettings pageSettings)
    {
        if (pageSettings is null) return;

        pageSettings.PageArrival();
    }
}
