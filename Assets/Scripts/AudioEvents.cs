using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Threading.Tasks;

public class AudioEvents : MonoBehaviour
{
    [SerializeField] private VoiceOver[] voiceTracks;
    [SerializeField] private AudioClip[] soundFxTracks;
    [SerializeField] private AudioClip[] ambientTracks;
    [SerializeField] private bool _playMusicOnStart;
    [SerializeField] private float _startMusicStartTime;
    [SerializeField] private float _startMusicDelay;

    [System.Serializable]
    private struct VoiceOver
    {
        public AudioClip track;
        public UnityEvent OnTrackEnd;
    }

    private void Awake()
    {
        if (!_playMusicOnStart) return;
        
        PlayAmbient(0);
    }

    public async void PlayVoiceOver(int index)
    {
        if (index >= voiceTracks.Length) return;

        AudioManager.Instance.Play(AudioManager.AudioSourceType.VoiceOver, voiceTracks[index].track);

        await Task.Yield();

        while (AudioManager.Instance.VoicerOverAS.isPlaying)
        {
            await Task.Yield();
        }

        voiceTracks[index].OnTrackEnd?.Invoke();
    }

    public void PlaySoundFx(int index)
    {
        if (index >= soundFxTracks.Length) return;

        AudioManager.Instance.Play(AudioManager.AudioSourceType.SoundFX, soundFxTracks[index]);
    }

    public void PlayAmbient(int index)
    {
        if (index >= ambientTracks.Length) return;

        AudioManager.Instance.Play(AudioManager.AudioSourceType.Ambient, ambientTracks[index], _startMusicStartTime, _startMusicDelay);
    }
}
