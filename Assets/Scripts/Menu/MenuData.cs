using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MenuData", menuName = "Menu/Data")]
public class MenuData : ScriptableObject
{
    [SerializeField] private Sprite[] _thumbnails;
    [SerializeField] private Sprite[] _volumeStages;

    public Sprite[] Thumbnails => _thumbnails;
    public Sprite[] VolumeStages => _volumeStages;
}
