using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;

[DefaultExecutionOrder(3)]
public class SaveMenuManager : MonoBehaviour
{
    [Header("Managers and references")]
    [SerializeField] private PageSwitchManager _pageSwitchManager;
    [SerializeField] private MenuData _menuData;
    [SerializeField] private Transform _menuContents;

    [Header("Sliders and Images")]
    [SerializeField] private Slider _saveProgressSlider;
    [SerializeField] private Slider _pageChoiceSlider;
    [SerializeField] private Slider _volumeSlider;
    [SerializeField] private Image _volumeSliderImage;
    [SerializeField] private Image _thumbnail;
    [SerializeField] private Image _fadeScreen;

    [Header("Buttons")]
    [SerializeField] private Sprite _soundOnSprite;
    [SerializeField] private Sprite _soundOffSprite;
    [SerializeField] private Image _soundFxImage;
    [SerializeField] private Image _voiceOverImage;

    private bool _soundFxOn = true;
    private bool _voiceOverOn = true;

    private SaveState _saveState;
    private int _currentPage;

    public static SaveMenuManager Instance { get; set; }

    public int CurrentPage
    {
        get { return _currentPage; }
        set
        {
            _currentPage = value;
            Save();
        }
    }

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        Load();

        //PlayerPrefs.DeleteKey("save");
    }

    public void Save()
    {
        _saveState.PageNum = CurrentPage;
        _saveProgressSlider.value = CurrentPage;
        PlayerPrefs.SetString("save", SaveHelper.Serialize<SaveState>(_saveState));
    }

    public void Load()
    {
        _fadeScreen.DOFade(0, 0.75f);

        if(PlayerPrefs.HasKey("save"))
        {
            // Deserialize class
            _saveState = SaveHelper.Deserialize<SaveState>(PlayerPrefs.GetString("save"));
            CurrentPage = _saveState.PageNum;
            _saveProgressSlider.value = CurrentPage;

            
            if (_saveState.StartPageNum != 0)
            {
                int nextPageNum;

                nextPageNum = _saveState.StartPageNum;
                _saveState.StartPageNum = 0;
                Save();
                _pageSwitchManager.SlidePageFromSaveFile(nextPageNum);
            }
            //print($"found a previous key with the value {_saveState.PageNum}");
        }
        else
        {
            _saveState = new SaveState();
            Save();
            //Debug.Log("No save file found. Creating new one");
        }
    }

    public void CheckPageChoice()
    {
        if (_pageChoiceSlider.value > _saveProgressSlider.value)
            _pageChoiceSlider.value = _saveProgressSlider.value;

        _thumbnail.sprite = _menuData.Thumbnails[(int)_pageChoiceSlider.value];
    }

    public void SetCurrentPage(int num)
    {
        if (num > CurrentPage)
            CurrentPage = num;
    }

    public void ChangeVolume(float value)
    {
        if (_soundFxOn)
        {
            AudioManager.Instance.AmbientAS.volume = value;
            AudioManager.Instance.AmbientAS2.volume = value;
            AudioManager.Instance.SoundFxAS.volume = value;
        }

        if (_voiceOverOn)
        {
            AudioManager.Instance.VoicerOverAS.volume = value;
        }

        if (value == 0) _volumeSliderImage.sprite = _menuData.VolumeStages[0];
        else if (value < 0.45f) _volumeSliderImage.sprite = _menuData.VolumeStages[1];
        else if (value < 0.99f) _volumeSliderImage.sprite = _menuData.VolumeStages[2];
        else _volumeSliderImage.sprite = _menuData.VolumeStages[3];
    }

    public void ChoosePage()
    {
        Sequence pageChooseSeq = DOTween.Sequence();

        pageChooseSeq
        .AppendCallback(() => _saveState.StartPageNum = (int)_pageChoiceSlider.value)
        .AppendCallback(() => Save())
        .Append(_fadeScreen.DOFade(1, 0.75f))
        .AppendCallback(() => SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex));
    }

    public void OpenMenu()
    {
        if (_menuContents.gameObject.activeSelf)
        {
            InputManager.Instance.EnableControls(true);
            _menuContents.gameObject.SetActive(false);
        }
        else
        {
            InputManager.Instance.EnableControls(false);
            _menuContents.gameObject.SetActive(true);
        }
    }

    public void TriggerSoundFx()
    {
        _soundFxOn = !_soundFxOn;

        if (_soundFxOn)
        {
            _soundFxImage.sprite = _soundOnSprite;
            AdjustAudio(_volumeSlider.value);
        }
        else 
        {
            _soundFxImage.sprite = _soundOffSprite;
            AdjustAudio(0);
        }

        void AdjustAudio(float value)
        {
            AudioManager.Instance.AmbientAS.volume = value;
            AudioManager.Instance.AmbientAS2.volume = value;
            AudioManager.Instance.SoundFxAS.volume = value;
        }
    }

    public void TriggerVoiceOver()
    {
        _voiceOverOn = !_voiceOverOn;

        if (_voiceOverOn)
        {
            _voiceOverImage.sprite = _soundOnSprite;
            AdjustAudio(_volumeSlider.value);
        }
        else 
        {
            _voiceOverImage.sprite = _soundOffSprite;
            AdjustAudio(0);
        }

        void AdjustAudio(float value)
        {
            AudioManager.Instance.VoicerOverAS.volume = value;
        }
    }
}
