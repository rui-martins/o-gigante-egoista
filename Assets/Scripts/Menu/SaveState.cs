public class SaveState
{
    public int PageNum { get; set; }
    public int StartPageNum { get; set; }

    public SaveState() => PageNum = 0;
}
