using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;
using System;
using UnityEngine.Events;

public class TextAnimEvent : MonoBehaviour, IPageEvent
{
    [System.Serializable]
    public struct TimeEvent
    {
        public float time;
        public UnityEvent OnTime;
    }

    [Header("Text properties")]
    [SerializeField] private TextMeshProUGUI[] allText;
    [SerializeField] private float[] lineTimer;
    [SerializeField] private float FadeSpeed = 100.0f;
    [SerializeField] private int RolloverCharacterSpread = 40;

    [Header("Text settings")]
    [SerializeField] private bool startOnPageLoad;

    [Header("Text Events")]
    public UnityEvent OnTextFadeStart;
    public UnityEvent OnTextFadeEnd;
    [SerializeField] private TimeEvent[] timeEvents;

    private List<(float time, UnityEvent animEvent)> tEvents = new List<(float, UnityEvent)>();

    public bool StartOnPageLoad => startOnPageLoad;
    public bool EventEnded { get; private set; }

    public void StartEvent()
    {
    }

    private void Start()
    {
        HideText();
        //StartCoroutine(FadeIn(0, 0));
        //StartCoroutine(ScaleWord(0));
        //StartCoroutine(ScaleWord2(0));
    }

    public void FadeIn()
    {
        StartCoroutine(FadeIn(0, 0));
        OnTextFadeStart?.Invoke();

        if (timeEvents.Length != 0)
            StartCoroutine(TimeEventRoutine());

    }

    private IEnumerator TimeEventRoutine()
    {
        PopulateTimeEventList();
        float currentAnimTime = 0;

        while (true)
        {
            if (tEvents.Count == 0)
            {
                break;
            }

            currentAnimTime += Time.deltaTime;

            for (int i = tEvents.Count - 1; i >= 0; i--)
            {
                if (currentAnimTime >= tEvents[i].time)
                {
                    tEvents[i].animEvent?.Invoke();
                    tEvents.RemoveAt(i);
                }
            }

            yield return null;
        }
    }

    private void PopulateTimeEventList()
    {
        tEvents.Clear();

        foreach (TimeEvent timeEvent
            in timeEvents)
        {
            tEvents.Add((timeEvent.time, timeEvent.OnTime));
        }
    }

    private void HideText()
    {
        foreach (TextMeshProUGUI text in allText)
        {
            // Set the whole text transparent
            text.color = new Color
                (
                    text.color.r,
                    text.color.g,
                    text.color.b,
                    0
                );

            // Need to force the text object to be generated so we have valid data to work with right from the start.
            text.ForceMeshUpdate();
        }
    }

    private IEnumerator ScaleWord(int textIndex)
    {
        bool flag = false;
        int testCounter = 0;

        TextMeshProUGUI text = allText[textIndex];
        TMP_TextInfo textInfo = text.textInfo;
        //text.ForceMeshUpdate();

        List<List<TMP_CharacterInfo>> words = new List<List<TMP_CharacterInfo>>();
        int listIndex = 0;

        float currentCharVertexOffset = 0;
        float currentCharZPosOffset = 0;
        float maxCharVertexOffset = 0.028f;
        float maxCharZPosOffset = 0.1f;

        Debug.Log(textInfo.characterCount);

        for (int i = 0; i < textInfo.characterCount; i++)
        {
            if (!flag)
            {
                testCounter = i;
                flag = true;
            }

            // If character is visible and end of text hasn't been reached
            if (textInfo.characterInfo[i].isVisible && i != textInfo.characterCount - 1)
            {
                continue;
            }
            else
            {
                flag = false;

                for (int j = testCounter; j < i; j++)
                {
                    int meshIndex = text.textInfo.characterInfo[j].materialReferenceIndex;
                    int vertexIndex = text.textInfo.characterInfo[j].vertexIndex;
                    Vector3[] vertices = text.textInfo.meshInfo[meshIndex].vertices;

                    Vector3[] initialVertexValues = new Vector3[4];
                    initialVertexValues[0] = vertices[vertexIndex + 0];
                    initialVertexValues[1] = vertices[vertexIndex + 1];
                    initialVertexValues[2] = vertices[vertexIndex + 2];
                    initialVertexValues[3] = vertices[vertexIndex + 3];

                    DOTween.Sequence()
                        .Append(DOTween.To(x => currentCharVertexOffset = x, 0f, maxCharVertexOffset, 0.1f))
                        .Join(DOTween.To(x => currentCharZPosOffset = x, 0f, maxCharZPosOffset, 0.1f))
                        .AppendInterval(0.2f)
                        .Append(DOTween.To(x => currentCharVertexOffset = x, maxCharVertexOffset, 0, 0.1f))
                        .Join(DOTween.To(x => currentCharZPosOffset = x, maxCharZPosOffset, 0, 0.1f))
                        .OnUpdate(() => ScaleChar(text, initialVertexValues, vertices, vertexIndex, currentCharVertexOffset, currentCharZPosOffset));
                }

                yield return new WaitForSeconds(0.3f);
            }
        }

        if (textIndex < allText.Length - 1)
            StartCoroutine(ScaleWord(textIndex + 1));
    }

    private void ScaleChar(TextMeshProUGUI text, Vector3[] initVertexVal, Vector3[] vertices, int vertexIndex, float charVertexOffset, float charZPosOffset)
    {
        Vector3 vertex1, vertex2, vertex3, vertex4;

        vertex1 = new Vector3(initVertexVal[0].x - charVertexOffset, initVertexVal[0].y - charVertexOffset, initVertexVal[0].z - charZPosOffset);
        vertex2 = new Vector3(initVertexVal[1].x - charVertexOffset, initVertexVal[1].y + charVertexOffset, initVertexVal[1].z - charZPosOffset);
        vertex3 = new Vector3(initVertexVal[2].x + charVertexOffset, initVertexVal[2].y + charVertexOffset, initVertexVal[2].z - charZPosOffset);
        vertex4 = new Vector3(initVertexVal[3].x + charVertexOffset, initVertexVal[3].y - charVertexOffset, initVertexVal[3].z - charZPosOffset);

        vertices[vertexIndex + 0] = vertex1;
        vertices[vertexIndex + 1] = vertex2;
        vertices[vertexIndex + 2] = vertex3;
        vertices[vertexIndex + 3] = vertex4;

        text.UpdateVertexData(TMP_VertexDataUpdateFlags.Vertices);
    }

    private IEnumerator ScaleWord2(int textIndex)
    {
        string test = "<size=0.80>";
        string test2 = "</size>";
        TextMeshProUGUI text = allText[textIndex];
        TMP_TextInfo textInfo = text.textInfo;
        bool flag = false;
        int testCounter = 0;

        text.ForceMeshUpdate();
        Debug.Log(textInfo.characterCount);

        for (int i = 0; i < textInfo.characterCount; i++)
        {
            string s = "";

            if (!flag)
            {
                flag = true;
                testCounter = i;
            }

            // If character is visible and end of text hasn't been reached
            if (textInfo.characterInfo[i].isVisible && i != textInfo.characterCount - 1)
            {
                continue;
            }
            else
            {
                flag = false;

                if (testCounter == i) continue;

                for (int j = 0; j < textInfo.characterCount; j++)
                {
                    if (testCounter == j)
                        s += test;

                    s += textInfo.characterInfo[j].character;

                    if (i == j)
                        s += test2;
                }

                text.text = s;
                yield return new WaitForSeconds(1f);
            }
        }

        if (textIndex < allText.Length - 1)
            StartCoroutine(ScaleWord2(textIndex + 1));
    }

    private IEnumerator FadeIn(int textIndex, int lineTimerIndex)
    {
        TextMeshProUGUI text = allText[textIndex];
        TMP_TextInfo textInfo = text.textInfo;
        TMP_LineInfo[] lineInfo = textInfo.lineInfo;
        Color32[] newVertexColors;

        int currentLine = 0;
        int currentCharacter = 0;
        int startingCharacterRange = currentCharacter;
        bool isRangeMax = false;
        bool startedNextLine = false;

        int characterCount = textInfo.characterCount;

        // Spread should not exceed the number of characters.
        byte fadeSteps = (byte)Mathf.Max(1, 255 / RolloverCharacterSpread);

        while (!isRangeMax)
        {
            for (int i = startingCharacterRange; i < currentCharacter + 1; i++)
            {
                // Skip characters that are not visible (like white spaces)
                if (!textInfo.characterInfo[i].isVisible) continue;

                // Get the index of the material used by the current character.
                int materialIndex = textInfo.characterInfo[i].materialReferenceIndex;

                // Get the vertex colors of the mesh used by this text element (character or sprite).
                newVertexColors = textInfo.meshInfo[materialIndex].colors32;

                // Get the index of the first vertex used by this text element.
                int vertexIndex = textInfo.characterInfo[i].vertexIndex;

                // Get the current character's alpha value.
                byte alpha = (byte)Mathf.Clamp(newVertexColors[vertexIndex + 0].a + fadeSteps, 0, 255);

                // Set new alpha values.
                newVertexColors[vertexIndex + 0].a = alpha;
                newVertexColors[vertexIndex + 1].a = alpha;
                newVertexColors[vertexIndex + 2].a = alpha;
                newVertexColors[vertexIndex + 3].a = alpha;

                if (alpha == 255)
                {
                    startingCharacterRange += 1;

                    if (startingCharacterRange >= lineInfo[currentLine].lastCharacterIndex)
                    {
                        yield return new WaitForSeconds(lineTimer[lineTimerIndex]);

                        lineTimerIndex++;
                        currentLine++;
                    }

                    if (startingCharacterRange == characterCount - 1)
                    {
                        // Update mesh vertex data one last time.
                        text.UpdateVertexData(TMP_VertexDataUpdateFlags.Colors32);

                        //yield return new WaitForSeconds(1.0f);

                        // Reset the text object back to original state.
                        //text.ForceMeshUpdate();

                        // Reset our counters.
                        currentCharacter = 0;
                        startingCharacterRange = 0;
                        isRangeMax = true; // Would end the coroutine.

                        if (lineTimerIndex <= lineTimer.Length - 1)
                        {
                            StartCoroutine(FadeIn(textIndex + 1, lineTimerIndex));
                        }
                        else
                        {
                            OnTextFadeEnd?.Invoke();
                        }
                    }
                }
            }
            // Upload the changed vertex colors to the Mesh.
            text.UpdateVertexData(TMP_VertexDataUpdateFlags.Colors32);

            if (currentLine >= lineInfo.Length) break;

            if (currentCharacter < lineInfo[currentLine].lastCharacterIndex)
            {
                if (currentCharacter + 1 < characterCount) currentCharacter += 1;
            }

            yield return new WaitForSeconds(0.25f - FadeSpeed * 0.01f);
        }
    }
}
