using UnityEngine;
using DG.Tweening;
using Spine.Unity;
using UnityEngine.Events;
using System.Collections;

public class SkeletonFade : MonoBehaviour
{
    [Header("Fade settings")]
    [SerializeField] private float startAlphaValue = 0;
    [SerializeField] private float endAlphaValue = 1;
    [SerializeField] private float fadeTime;

    [Header("Fade Events")]
    [SerializeField] private float onEndEventDelay = 0f;
    public UnityEvent OnTexFadeEnd;

    private SkeletonAnimation skeletonAnim;
    private Texture tex;
    private float currentAlphaValue;

    private void Start()
    {
        skeletonAnim = GetComponent<SkeletonAnimation>();

        if (skeletonAnim is null) return;

        skeletonAnim.skeleton.A = startAlphaValue;
        currentAlphaValue = startAlphaValue;
    }

    public void Fade()
    {
        DOTween.Sequence()
        .Append(DOTween.To(() => currentAlphaValue, x => currentAlphaValue = x, endAlphaValue, fadeTime))
        .OnUpdate(() => skeletonAnim.skeleton.A = currentAlphaValue)
        .OnComplete(() => StartCoroutine(RaiseEventRoutine(OnTexFadeEnd, onEndEventDelay)));
    }

    private IEnumerator RaiseEventRoutine(UnityEvent texEvent, float delay)
    {
        if (delay > 0) yield return new WaitForSeconds(delay);

        texEvent?.Invoke();
        yield return null;
    }
}
