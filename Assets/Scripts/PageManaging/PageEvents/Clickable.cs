using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Clickable : MonoBehaviour
{
    [SerializeField] private bool interactable;
    [SerializeField] private bool oneTimeInteract = true;
    public UnityEvent OnClick;

    public void Click()
    {
        if (interactable)
        {
            if (oneTimeInteract) interactable = false;

            OnClick?.Invoke();
            HintManager.Instance.HideHint();
        }
    }

    public void SetInteractivity(bool value)
    {
        interactable = value;
    }
}