using UnityEngine;
using DG.Tweening;

public class CamShake : MonoBehaviour
{
    [SerializeField] private float duration;
    [SerializeField] private float strength;
    [SerializeField] private int vibrato;

    private Camera mainCam;

    private void Awake()
    {
        mainCam = Camera.main;
    }

    public void Shake()
    {
        mainCam.DOShakePosition(duration, strength, vibrato);
    }
}
