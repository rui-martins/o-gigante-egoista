using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnityAnimEvent : MonoBehaviour
{
    [SerializeField] private Animator animator;

    public void SetTrigger(string state)
    {
        animator.SetTrigger(state);
    }
}
