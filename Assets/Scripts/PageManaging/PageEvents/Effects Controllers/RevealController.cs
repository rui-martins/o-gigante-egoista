using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class RevealController : MonoBehaviour
{
    [System.Serializable]
    public struct RevealProperties
    {
        public float sliderValue;
        public float time;
    }

    [SerializeField] private RevealProperties[] revealProperties;

    private Material mat;
    private float sliderInitValue;

    public UnityEvent OnRevealEnd;

    private void Awake()
    {
        mat = GetComponent<Renderer>().material;
        sliderInitValue = mat.GetFloat("_Reveal");
    }

    public void Reveal()
    {
        float t = 0;

        foreach (RevealProperties revealProp in revealProperties)
        {
            t += revealProp.time;
            StartCoroutine(RevealRoutine(revealProp.sliderValue, t));
        }
        StartCoroutine(RevealEndEvent(t));
    }

    private IEnumerator RevealRoutine(float sliderValue, float time)
    {
        yield return new WaitForSeconds(time);
        mat.SetFloat("_Reveal", sliderValue);
    }

    private IEnumerator RevealEndEvent(float t)
    {
        yield return new WaitForSeconds(t);
        OnRevealEnd?.Invoke();
    }
}
