using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TimedEvent : MonoBehaviour
{
    [System.Serializable]
    public struct TimeEvent
    {
        public float time;
        public UnityEvent OnTimeReach;
    }

    [SerializeField] private TimeEvent[] timeEvents;

    private List<(float time, UnityEvent animEvent)> tEvents = new List<(float, UnityEvent)>();

    public void StartTimeEvents()
    {
        PopulateTimeEventList();
        StartCoroutine(EventTimer());
    }

    private void PopulateTimeEventList()
    {
        foreach (TimeEvent timeEvent in timeEvents)
        {
            tEvents.Add((timeEvent.time, timeEvent.OnTimeReach));
        }
    }

    private IEnumerator EventTimer()
    {
        float currentAnimTime = 0;

        while (true)
        {
            if (tEvents.Count == 0)
            {
                break;
            }

            currentAnimTime += Time.deltaTime;

            for (int i = tEvents.Count - 1; i >= 0; i--)
            {
                if (currentAnimTime >= tEvents[i].time)
                {
                    tEvents[i].animEvent?.Invoke();
                    tEvents.RemoveAt(i);
                }
            }
            yield return null;
        }
    }
}
