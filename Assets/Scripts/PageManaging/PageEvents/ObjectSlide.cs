using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;
using System.Collections;

public class ObjectSlide : MonoBehaviour
{
    [System.Serializable]
    public struct MoveValues
    {
        public Vector3 moveVector;
        public float duration;
        public float endWaitTime;
        public UnityEvent OnSlideEnd;
    }

    [SerializeField] private Transform targetObject;
    [SerializeField] private MoveValues[] moveValues;

    private Queue<MoveValues> pathQueue = new Queue<MoveValues>();
    private Sequence moveSeq;
    private Vector3 targetPos = Vector3.zero;

    private Queue<Sequence> AnimationQueue = new Queue<Sequence>();

    private void Awake()
    {
        moveSeq = DOTween.Sequence();

        foreach (MoveValues moveVals in moveValues)
        {
            pathQueue.Enqueue(moveVals);
        }
    }

    public void Slide()
    {
        MoveValues moveVals = pathQueue.Dequeue();
        if (targetPos == Vector3.zero)
            targetPos = transform.position + moveVals.moveVector;
        else
            targetPos += moveVals.moveVector;

        PlayQueuedAnimation(targetPos, moveVals.duration, moveVals.endWaitTime, moveVals.OnSlideEnd);
    }

    public void SlideAll()
    {
        int pathQueueSize = pathQueue.Count;

        for (int i = 0; i < pathQueueSize; i++)
        {
            Slide();
        }
    }

    //Entry point
    public void PlayQueuedAnimation(Vector3 targetPos, float duration, float endWaitTime, UnityEvent OnEvent)
    {
        //Create paused sequence 
        var seq = DOTween.Sequence();
        seq.Pause();

        //Append everything you want
        seq.Append(targetObject.DOLocalMove(targetPos, duration));
        seq.AppendInterval(endWaitTime);
        seq.AppendCallback(() => OnEvent?.Invoke());
        //...

        //Add to queue
        AnimationQueue.Enqueue(seq);

        //Check if this animation is first in queue
        if (AnimationQueue.Count == 1)
        {
            AnimationQueue.Peek().Play();
        }

        //Set callback 
        seq.OnComplete(OnComplete);
    }

    //Callback
    private void OnComplete()
    {
        //remove animation that was completed
        AnimationQueue.Dequeue();

        //if there's animations in queue left
        if (AnimationQueue.Count > 0)
        {
            //play next
            AnimationQueue.Peek().Play();
        }
    }
}
