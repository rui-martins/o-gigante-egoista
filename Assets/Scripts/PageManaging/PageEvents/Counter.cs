using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Counter : MonoBehaviour
{
    [SerializeField] private int counterLimit;

    public UnityEvent[] onCountUpdate;

    public UnityEvent OnCountReached;

    private int currentValue;

    private void Awake()
    {
        currentValue = 0;
    }

    public void UpdateCounter(int n)
    {
        if (currentValue < 0 || currentValue > counterLimit) return;

        onCountUpdate[currentValue]?.Invoke();

        currentValue += n;

        if (currentValue >= counterLimit)
            OnCountReached?.Invoke();
    }
}
