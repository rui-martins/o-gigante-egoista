using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using UnityEngine.Events;
using System.Linq;

[DefaultExecutionOrder(1)]
public class SpineAnimEvent : MonoBehaviour, IPageEvent
{
    [Serializable]
    public class SpineAnimEventState
    {
        public AnimationReferenceAsset animAsset;
        public string state;
        public float startFrame = 0f;
        public float timeScale = 1f;
        public bool fireStartEventOnLoop = false;
        public UnityEvent OnAnimStart;
        public bool fireEndEventOnLoop = false;
        public UnityEvent OnAnimEnd;
        public bool fireTimeEventsOnLoop = false;
        public AnimationTimeEvent[] animTimeEvent;

        [Serializable]
        public struct AnimationTimeEvent
        {
            public float time;
            public UnityEvent OnAnimTime;
        }

        public bool IsLooping { get; private set; }

        public void SetLoopState(bool loop) => IsLooping = loop;
    }

    [Header("Event properties")]
    [SerializeField] private SkeletonAnimation skeletonAnim;
    [SerializeField] private SpineAnimEventState[] animEventStates;

    [Header("Event settings")]
    [SerializeField] private bool startOnPageLoad = true;
    [SerializeField] private float startOnPageLoadDelay = 0f;
    [SerializeField] private bool canInterrupt = false;
    [SerializeField] private bool loopOnLoad = false;

    private List<(float time, UnityEvent animEvent)> tEvents = new List<(float, UnityEvent)>();
    private Coroutine animTimer;
    private SpineAnimEventState currentAnimationState;
    private bool isPlaying;
    private bool listenersSet;

    // Debug
    private float currentAnimTime;

    public bool StartOnPageLoad => startOnPageLoad;
    public bool EventEnded { get; private set; }

    private void Start()
    {
        if (skeletonAnim is null) return;
        if (animEventStates.Length == 0) return;

        currentAnimationState = animEventStates[0];

        AddListeners();

        if (startOnPageLoad)
        {
            StartCoroutine(LoadInitialAnimRoutine(startOnPageLoadDelay));
        }
    }

    public void SetInterruptMode(bool val)
    {
        canInterrupt = val;
    }    

    private IEnumerator LoadInitialAnimRoutine(float time)
    {
        yield return new WaitForSeconds(time);

        if (loopOnLoad)
            SetAnimationStateLoop(currentAnimationState.state);
        else
            SetAnimationStateNoLoop(currentAnimationState.state);
    }

    private void SetAnimation(AnimationReferenceAsset animation, bool loop, float timeScale = 1f)
    {
        if (skeletonAnim.timeScale != timeScale)
            skeletonAnim.timeScale = timeScale;

        Spine.TrackEntry tEntry = skeletonAnim.state.SetAnimation(0, animation, loop);
        tEntry.TimeScale = timeScale;

        float startFrame;

        if ((startFrame = currentAnimationState.startFrame) > 0)
        {
            tEntry.TimeScale = 0;
            float animTimeInSeconds = startFrame / 30f;
            tEntry.TrackTime = animTimeInSeconds;
            tEntry.TimeScale = timeScale;
        }
    }

    public void SwitchLoopState(bool loop)
    {
        currentAnimationState.SetLoopState(loop);

        Spine.TrackEntry tEntry = skeletonAnim.state.GetCurrent(0);

        StartCoroutine(SwitchLoopStateRoutine(tEntry, loop));

        if (tEntry.Loop != loop)
            tEntry.Loop = loop;
    }

    public void ChangeTimeScale(float time)
    {
        skeletonAnim.timeScale = time;
    }

    private IEnumerator SwitchLoopStateRoutine(Spine.TrackEntry tEntry, bool loop)
    {
        WaitForSpineTrackEntryEnd waitForTrackEnd = new WaitForSpineTrackEntryEnd(tEntry);
        yield return waitForTrackEnd;

        if (tEntry.Loop != loop)
            tEntry.Loop = loop;
    }

    public void SetAnimationStateNoLoop(string state) => SetAnimationState(state, false);
    public void SetAnimationStateLoop(string state) => SetAnimationState(state, true);

    public void SwitchFireEndEventOnLoop(bool val) => currentAnimationState.fireEndEventOnLoop = val;

    public void SetAnimationState(string state, bool loop)
    {
        if (isPlaying && !canInterrupt) return;

        foreach (SpineAnimEventState eventState in animEventStates)
        {
            if (state == eventState.state)
            {
                currentAnimationState = eventState;
                PopulateTimeEventList();
                currentAnimationState.SetLoopState(loop);
                // CHANGED LOOP TO FALSE
                SetAnimation(eventState.animAsset, false, eventState.timeScale);
            }
        }
    }

    private void PopulateTimeEventList()
    {
        tEvents.Clear();

        foreach (SpineAnimEventState.AnimationTimeEvent timeEvent
            in currentAnimationState.animTimeEvent)
        {
            tEvents.Add((timeEvent.time, timeEvent.OnAnimTime));
        }
    }

    public void StartEvent()
    {
    }

    private void OnAnimStart(Spine.TrackEntry trackEntry)
    {
        if (animTimer != null)
            animTimer = null;

        isPlaying = true;

        if ((currentAnimationState.IsLooping && currentAnimationState.fireStartEventOnLoop) || !currentAnimationState.IsLooping)
            animEventStates.First((eventState) =>
                eventState.animAsset.name == trackEntry.Animation.Name).OnAnimStart?.Invoke();

        if (currentAnimationState.fireTimeEventsOnLoop && currentAnimationState.animTimeEvent.Length != 0 || !currentAnimationState.IsLooping)
        {
            PopulateTimeEventList();
            animTimer = StartCoroutine(AnimationTimer(trackEntry));
        }
        else
            animTimer = null;
    }

    private void OnAnimEnd(Spine.TrackEntry trackEntry)
    {
    }

    private void OnAnimInterrupt(Spine.TrackEntry trackEntry)
    {
        if (isPlaying)
        {
            if (animTimer != null)
                StopCoroutine(animTimer);
        }
    }

    private void OnAnimComplete(Spine.TrackEntry trackEntry)
    {
        if (animTimer != null)
            StopCoroutine(animTimer);

        // FAILSAFE - If, for some reason time event didn't fire, we call what's left on the list

        if (tEvents.Count > 0)
            for (int i = tEvents.Count - 1; i >= 0; i--)
            {
                tEvents[i].animEvent?.Invoke();
                tEvents.RemoveAt(i);
            }

        if (!currentAnimationState.IsLooping)
        {
            isPlaying = false;
            animEventStates.First((eventState) =>
                eventState.animAsset.name == trackEntry.Animation.Name).OnAnimEnd?.Invoke();
        }
        else
        {
            skeletonAnim.state.AddAnimation(0, currentAnimationState.animAsset.name, false, 0f);

            if (currentAnimationState.fireEndEventOnLoop)
                animEventStates.First((eventState) =>
                    eventState.animAsset.name == trackEntry.Animation.Name).OnAnimEnd?.Invoke();
        }
    }

    private void AddListeners()
    {
        skeletonAnim.state.Start += OnAnimStart;
        skeletonAnim.state.Interrupt += OnAnimInterrupt;
        skeletonAnim.state.Complete += OnAnimComplete;
        skeletonAnim.state.End += OnAnimEnd;

        listenersSet = true;
    }


    private void RemoveListeners()
    {
        if (listenersSet)
        {
            skeletonAnim.state.Start -= OnAnimStart;
            skeletonAnim.state.Interrupt -= OnAnimInterrupt;
            skeletonAnim.state.Complete -= OnAnimComplete;
            skeletonAnim.state.End -= OnAnimEnd;
        }
    }

    private void OnDisable()
    {
        RemoveListeners();
    }

    private IEnumerator AnimationTimer(Spine.TrackEntry tEntry)
    {
        currentAnimTime = 0;

        while (true)
        {
            if (tEvents.Count == 0)
            {
                break;
            }

            currentAnimTime = tEntry.TrackTime;
            //Debug.Log(currentAnimTime);
            for (int i = tEvents.Count - 1; i >= 0; i--)
            {
                if (currentAnimTime >= tEvents[i].time)
                {
                    //Debug.Log("GOt hereeee and my name is" + gameObject.name);
                    tEvents[i].animEvent?.Invoke();
                    tEvents.RemoveAt(i);
                }
            }

            yield return null;
        }
    }

    public void PlayRandomAnim(bool loop)
    {
        int n = UnityEngine.Random.Range(0, animEventStates.Length);
        string state = animEventStates[n].state;

        SetAnimationState(state, loop);
    }

    public void DebugLog(string s)
    {
        Debug.Log(s);
    }
}
