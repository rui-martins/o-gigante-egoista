using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;
using System.Linq;

public class PageSettings : MonoBehaviour
{
    [System.Serializable]
    public struct TimeEvent
    {
        public float time;
        public UnityEvent OnTimeReach;
    }

    [Header("General Settings")]
    [SerializeField] private float transitionSpeed;
    [SerializeField] private float pageDistance;

    [Header("Transition events")]
    public UnityEvent OnTransitionStart;

    [Header("Events settings")]
    [SerializeField] private float onPageArrivalDelay;

    private ParticleSystem hintPart;

    private List<(float time, UnityEvent animEvent)> tEvents = new List<(float, UnityEvent)>();

    public UnityEvent OnPageArrival;
    public UnityEvent OnAwake;
    public TimeEvent[] timeEvents;

    public float TransitionSpeed => transitionSpeed;
    public float PageDistance => pageDistance;

    private void Start()
    {
        PopulateTimeEventList();
    }

    private void Awake()
    {
        OnAwake?.Invoke();
    }

    public void PageArrival()
    {
        hintPart = GetComponentsInChildren<ParticleSystem>().First(p => p.tag == "Hint");
        HintManager.Instance.HintParticles = hintPart;

        DOTween.Sequence()
            .AppendInterval(onPageArrivalDelay)
            .AppendCallback(() => OnPageArrival?.Invoke())
            .AppendCallback(() => StartCoroutine(EventTimer()));
    }
    
    private void PopulateTimeEventList()
    {
        foreach (TimeEvent timeEvent in timeEvents)
        {
            tEvents.Add((timeEvent.time, timeEvent.OnTimeReach));
        }
    }

    private IEnumerator EventTimer()
    {
        float currentAnimTime = 0;

        while (true)
        {
            if (tEvents.Count == 0)
            {
                break;
            }

            currentAnimTime += Time.deltaTime;

            for (int i = tEvents.Count - 1; i >= 0; i--)
            {
                if (currentAnimTime >= tEvents[i].time)
                {
                    tEvents[i].animEvent?.Invoke();
                    tEvents.RemoveAt(i);
                }
            }
            yield return null;
        }
    }
}
