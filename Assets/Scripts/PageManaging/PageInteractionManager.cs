using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using UnityEngine.Events;

[DefaultExecutionOrder(-1)]
public class PageInteractionManager : MonoBehaviour
{
    //private IPageEvent[] pageEvents;

    private void Awake()
    {
        //pageEvents = GetComponentsInChildren<IPageEvent>();
    }

    private void OnEnable()
    {
        InputManager.Instance.OnClick += HandleClick;
    }

    private void OnDisable()
    {
        InputManager.Instance.OnClick -= HandleClick;
    }

    public void StartEvents()
    {
        //foreach (IPageEvent pEvent in pageEvents)
            //if (pEvent.StartOnPageLoad) pEvent.StartEvent();
    }

    private void HandleClick(Ray clickRay)
    {
        //Debug.Log("Click");
        /*
        Debug.DrawLine(pos, Vector3.forward * 5f, Color.red, 4f);

        RaycastHit rayHit;

        if (Physics.Raycast(pos, Vector3.forward, out rayHit, 100f))
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(pos, (pos + Vector3.forward) * 100f);
        } */

        Debug.DrawLine(clickRay.origin, clickRay.direction * 50f, Color.red, 4f);

        RaycastHit2D[] hits = Physics2D.GetRayIntersectionAll(clickRay);

        foreach (RaycastHit2D hit in hits)
        {
            Clickable clickable = hit.collider.GetComponent<Clickable>();

            if (clickable != null)
            {
                clickable.Click();
            }
        }
    }
}
