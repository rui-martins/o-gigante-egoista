using System;

public interface IPageEvent
{
    public bool StartOnPageLoad { get; }
    public bool EventEnded { get; }
    public void StartEvent();
}
