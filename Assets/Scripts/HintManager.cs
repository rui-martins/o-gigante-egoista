using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;

public class HintManager : MonoBehaviour
{
    [SerializeField] private float timeForHint;
    public static HintManager Instance {get; private set; }

    public ParticleSystem HintParticles {get; set;}

    private void Awake() 
    {
        Instance = this;   
    }

    public void ShowHint()
    {
        StartCoroutine(Delay(timeForHint));

        IEnumerator Delay(float delay)
        {
            yield return new WaitForSeconds(delay);

            if (HintParticles != null)
                HintParticles.Play();
        }

    }

    public void HideHint()
    {
        if (HintParticles != null)
            HintParticles.Stop();
        
        HintParticles = null;
    }
}
