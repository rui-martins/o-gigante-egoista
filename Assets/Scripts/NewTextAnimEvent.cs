using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;
using UnityEngine.Events;

public class NewTextAnimEvent : MonoBehaviour
{
    [System.Serializable]
    public struct TimeEvent
    {
        public float time;
        public UnityEvent OnTime;
    }

    [Header("Text properties")]
    [SerializeField] private TextMeshProUGUI[] allText;
    [SerializeField] private float[] blockTimer;
    [SerializeField] private float fadeTime = 2f;

    public UnityEvent OnTextFadeStart;
    public UnityEvent OnTextFadeEnd;
    [SerializeField] private TimeEvent[] timeEvents;

    private List<(float time, UnityEvent animEvent)> tEvents = new List<(float, UnityEvent)>();

    private List<Material> matList = new List<Material>();

    private void Start()
    {
        foreach (TextMeshProUGUI text in allText)
            matList.Add(text.fontMaterial);

        HideText();
    }

    private void HideText()
    {
        foreach (Material mat in matList)
            mat.SetFloat(ShaderUtilities.ID_FaceDilate, -1f);
    }

    public void FadeIn()
    {
        OnTextFadeStart?.Invoke();
        StartCoroutine(FadeRoutine(0));
        StartCoroutine(TimeEventRoutine());
    }

    private IEnumerator FadeRoutine(int textIndex)
    {
        if (textIndex < allText.Length)
        {
            matList[textIndex].DOFloat(0f, ShaderUtilities.ID_FaceDilate, fadeTime);
            yield return new WaitForSeconds(blockTimer[textIndex]);

            StartCoroutine(FadeRoutine(textIndex + 1));
        }

        OnTextFadeEnd?.Invoke();
    }

    private IEnumerator TimeEventRoutine()
    {
        PopulateTimeEventList();
        float currentAnimTime = 0;

        while (true)
        {
            if (tEvents.Count == 0)
            {
                break;
            }

            currentAnimTime += Time.deltaTime;

            for (int i = tEvents.Count - 1; i >= 0; i--)
            {
                if (currentAnimTime >= tEvents[i].time)
                {
                    tEvents[i].animEvent?.Invoke();
                    tEvents.RemoveAt(i);
                }
            }

            yield return null;
        }
    }

    private void PopulateTimeEventList()
    {
        tEvents.Clear();

        foreach (TimeEvent timeEvent
            in timeEvents)
        {
            tEvents.Add((timeEvent.time, timeEvent.OnTime));
        }
    }
}
