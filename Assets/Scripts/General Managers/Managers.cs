using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DefaultExecutionOrder(0)]
public class Managers : MonoBehaviour
{
    public static PageInteractionManager PageManager { get; private set; }
    public static InputManager InputManager { get; private set; }

    public static Camera MainCam { get; private set; }

    private void Awake()
    {
        PageManager = FindObjectOfType<PageInteractionManager>();
        InputManager = InputManager.Instance;
        MainCam = Camera.main;

        //QualitySettings.vSyncCount = 0;
        //Application.targetFrameRate = 24;
    }
}
