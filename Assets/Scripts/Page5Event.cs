using UnityEngine;
using System.Threading.Tasks;
using UnityEngine.Events;

public class Page5Event : MonoBehaviour
{
    private bool footstepsEnded = false;

    public UnityEvent OnInteractionEnd;

    public async void Pop()
    {
        await EndPage();
    }

    public void SetFootsteps(bool val) => footstepsEnded = val;

    private async Task EndPage()
    {
        while (!footstepsEnded)
        {
            await Task.Yield();
        }
        OnInteractionEnd?.Invoke();
    }
}
