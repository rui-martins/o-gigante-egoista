using UnityEngine;

public static class MyUnityExtensions
{
    public static Transform[] GetAllChilds(this Transform t)
    {
        Transform[] childs = new Transform[t.childCount];

        for (int i = 0; i < childs.Length; i++)
        {
            childs[i] = t.GetChild(i);
        }

        return childs;
    }
}
