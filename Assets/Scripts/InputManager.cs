using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using System;
using UnityEngine.EventSystems;

[DefaultExecutionOrder(-2)]
public class InputManager : MonoBehaviour
{
    [SerializeField] private float touchClickThreshold;

    private static InputManager instance;

    private PlayerControls pControls;

    private Vector2 startTouchPos, endTouchPos;

    public delegate void StartTouch(Vector2 position, float time);
    public event StartTouch OnStartTouch;
    public delegate void EndTouch(Vector2 position, float time);
    public event EndTouch OnEndTouch;

    public delegate void Click(Ray ray);
    public event Click OnClick;

    public static InputManager Instance { get { CheckInstance(); return instance; } }

    private void Awake()
    {
        instance = this;
        pControls = new PlayerControls();
    }

    public void EnableControls(bool value)
    {
        if (value) pControls.Enable();
        else pControls.Disable();
    }

    private void OnEnable()
    {
        pControls.Enable();
    }

    private void OnDisable()
    {
        pControls.Disable();
    }

    private void Start()
    {
        pControls.Touch.PrimaryContact.started += ctx => StartTouchPrimary(ctx);
        pControls.Touch.PrimaryContact.canceled += ctx => EndTouchPrimary(ctx);
    }

    private void StartTouchPrimary(InputAction.CallbackContext ctx)
    {
        Vector2 currentPos = pControls.Touch.PrimaryPosition.ReadValue<Vector2>();
        Vector3 pos3D = new Vector3(currentPos.x, currentPos.y, -Managers.MainCam.transform.position.z);
        OnStartTouch?.Invoke(Managers.MainCam.ScreenToWorldPoint(pos3D), (float)ctx.startTime);
        //OnStartTouch?.Invoke(currentPos, (float)ctx.startTime);

        startTouchPos = pos3D;
    }

    private void EndTouchPrimary(InputAction.CallbackContext ctx)
    {
        Vector2 currentPos = pControls.Touch.PrimaryPosition.ReadValue<Vector2>();
        Vector3 pos3D = new Vector3(currentPos.x, currentPos.y, -Managers.MainCam.transform.position.z);
        OnEndTouch?.Invoke(Managers.MainCam.ScreenToWorldPoint(pos3D), (float)ctx.time);
        //OnEndTouch?.Invoke(currentPos, (float)ctx.time);

        endTouchPos = pos3D;

        if (EventSystem.current.IsPointerOverGameObject()) return;
        
        if (Vector2.Distance(startTouchPos, endTouchPos) < touchClickThreshold)
        {
            OnClick?.Invoke(Managers.MainCam.ScreenPointToRay(pos3D));
        }
    }

    public Vector2 GetTouchPosition(PositionType posType)
    {
        Vector2 currentPos = pControls.Touch.PrimaryPosition.ReadValue<Vector2>();
        Vector3 pos3D = new Vector3(currentPos.x, currentPos.y, -Managers.MainCam.transform.position.z);

        switch (posType)
        {
            case PositionType.World:
                return Managers.MainCam.ScreenToWorldPoint(pos3D);
            case PositionType.Screen:
                return pos3D;
            case PositionType.Viewport:
                return Managers.MainCam.ScreenToViewportPoint(pos3D);
        }
        //Debug.Log(mainCam.ScreenToWorldPoint(pos3D));

        return Vector2.zero;
    }

    
    private static void CheckInstance()
    {
        if (instance)
            return;

        instance = new GameObject("Input Manager").AddComponent<InputManager>();
        DontDestroyOnLoad(instance.gameObject);
    }
}
