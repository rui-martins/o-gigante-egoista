using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DefaultExecutionOrder(2)]
public class Swingable : MonoBehaviour
{
    [SerializeField] private float moveSpeed;
    [SerializeField] private float maxLeftRotation;
    [SerializeField] private float maxRightRotation;
    [SerializeField] private bool isInteractable;

    private Camera cam;
    private Rigidbody2D rb;
    private HingeJoint2D joint;
    private bool movingClockwise;
    private bool isMoving;

    private Vector2 initialTouchPos;
    private Vector2 currentTouchPos;

    private Coroutine moveRoutine;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        joint = GetComponent<HingeJoint2D>();
        cam = Camera.main;
    }

    private void OnEnable()
    {
        Managers.InputManager.OnStartTouch += StartTouch;
        Managers.InputManager.OnEndTouch += EndTouch;
    }

    private void OnDisable()
    {
        Managers.InputManager.OnStartTouch -= StartTouch;
        Managers.InputManager.OnEndTouch -= EndTouch;
    }

    private void StartTouch(Vector2 position, float time)
    {
        initialTouchPos = position;
        //rb.isKinematic = true;

        Vector3 touchScreenPoint = cam.WorldToScreenPoint(position);
        Ray touchRay = cam.ScreenPointToRay(touchScreenPoint);
        RaycastHit2D[] hits = Physics2D.GetRayIntersectionAll(touchRay);

        foreach (RaycastHit2D hit in hits)
        {
            Swingable swingable = hit.collider.GetComponent<Swingable>();

            if (swingable != null && swingable.transform == transform)
            {
                isMoving = true;
            }
        }

        //moveRoutine = StartCoroutine(RotateTo());
    }

    private void EndTouch(Vector2 position, float time)
    {
        //rb.isKinematic = false;
        isMoving = false;

        //StopCoroutine(moveRoutine);
    }

    private void FixedUpdate()
    {
        if (rb.rotation <= maxLeftRotation)
        {
            isMoving = false;
            rb.angularVelocity = 0f;
            rb.velocity = Vector3.zero;
        }
        else if (rb.rotation >= maxRightRotation)
        {
            isMoving = false;
            rb.angularVelocity = 0f;
            rb.velocity = Vector3.zero;
        }
        else if (isMoving && isInteractable)
            Move();
    }

    private void Move()
    {
        currentTouchPos = Managers.InputManager.GetTouchPosition(PositionType.World);

        //float distance = Mathf.Abs(transform.position.x - currentTouchPos.x);

        float angle = 10f;
        float distance = Mathf.Abs(transform.position.x - currentTouchPos.x);

        if (currentTouchPos.x > initialTouchPos.x)
        {
            if (Vector3.Angle(transform.right, (Vector2)transform.position - currentTouchPos) > angle)
                rb.angularVelocity = distance * moveSpeed;
        }
        else if (currentTouchPos.x < initialTouchPos.x)
        {
            if (Vector3.Angle(transform.right, (Vector2)transform.position - currentTouchPos) > angle)
                rb.angularVelocity = -distance * moveSpeed;
        }
        else
        {
            rb.angularVelocity = 1f;
        }


        //initialTouchPos = currentTouchPos;
    }

    private IEnumerator RotateTo()
    {
        currentTouchPos = Managers.InputManager.GetTouchPosition(PositionType.World);
        Vector3 targetDirection = ((Vector3)currentTouchPos - transform.position).normalized;
        Quaternion targetRotation = Quaternion.LookRotation(targetDirection);

        while (Quaternion.Angle(transform.rotation, targetRotation) > 0.01f)
        {
            //transform.rotation = targetDirection;
            yield return null;
        }
    }

    // THIS SHOULD BE IN A SEPARATE SCRIPT.
    // 
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("got here");
        SpineAnimEvent spineAnimEvent = collision.transform.GetComponent<SpineAnimEvent>();

        if (spineAnimEvent != null && Mathf.Abs(rb.velocity.magnitude) > 0.0000005f)
        {
            spineAnimEvent.SetAnimationStateNoLoop("Break");
        }
    }

    public void IsMoving(bool val) => isMoving = val;

    public void SetInteractivity(bool val) => isInteractable = val;
}
