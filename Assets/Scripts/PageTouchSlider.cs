using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[DefaultExecutionOrder(1)]
public class PageTouchSlider : MonoBehaviour
{
    [SerializeField] private bool lockOnDestination;
    [SerializeField] private float lockOffset;
    [SerializeField] private float destinationY;
    [SerializeField] private float maxY;
    [SerializeField] private float minY;
    [SerializeField] [Range(1f, 5f)] private float smoothing;

    public UnityEvent OnDestinationReached;

    private Coroutine slideRoutine;
    private Vector2 initialTouchPos;
    private InputManager inputManager;
    private bool isStopping;
    private bool destinationReached;

    private void Awake()
    {
        inputManager = InputManager.Instance;
    }

    public void EnableTouchSlider()
    {
        inputManager.OnStartTouch += StartSlide;
        inputManager.OnEndTouch += EndSlide;
    }

    private void DisableTouchSlider()
    {
        inputManager.OnStartTouch -= StartSlide;
        inputManager.OnEndTouch -= EndSlide;
    }

    private void StartSlide(Vector2 position, float time)
    {
        if (slideRoutine != null)
        {
            StopCoroutine(slideRoutine);
        }

        initialTouchPos = position;
        isStopping = false;
        slideRoutine = StartCoroutine(Slide());
    }

    private void EndSlide(Vector2 position, float time)
    {
        isStopping = true;
    }

    private IEnumerator Slide()
    {
        Vector2 currentTouchPos = Vector2.zero;
        Vector3 desiredPos = Vector3.zero;
        float slideDistance = 0;
        float stoppingTimeLerped = 0f;

        while (true)
        {
            if (!isStopping)
                currentTouchPos = inputManager.GetTouchPosition(PositionType.World);

            if (Mathf.Abs(transform.position.y - destinationY) < lockOffset && !destinationReached)
            {
                destinationReached = true;

                if (lockOnDestination)
                    DisableTouchSlider();

                OnDestinationReached?.Invoke();
                isStopping = true;
            }

            if (isStopping)
            {
                stoppingTimeLerped += Time.deltaTime;
                slideDistance = Mathf.Lerp(slideDistance, 0, stoppingTimeLerped / 10f);
                if (slideDistance < 1) break;
            }
            else
            {
                slideDistance = Mathf.Abs(initialTouchPos.y - currentTouchPos.y);
            }

            if (currentTouchPos.y > initialTouchPos.y)
            {
                desiredPos = new Vector3(transform.position.x, maxY, transform.position.z);
                transform.Translate(Vector3.up * Time.deltaTime * (slideDistance / smoothing));
            }
            else
            {
                desiredPos = new Vector3(transform.position.x, minY, transform.position.z);
                transform.Translate(Vector3.down * Time.deltaTime * (slideDistance / smoothing));
            }

            Vector3 myPos = transform.position;
            myPos.y = Mathf.Clamp(myPos.y, minY, maxY);
            transform.position = myPos;

            //transform.position = Vector3.Lerp(transform.position, desiredPos, Time.deltaTime / (10f / slideDistance));
            //transform.position = Vector3.MoveTowards(transform.position, desiredPos, Time.deltaTime * (slideDistance  / smoothing));

            yield return null;
        }
    }
}
