using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class FadeImage : MonoBehaviour
{
    [SerializeField] private SpriteRenderer _imgToFade; 
    [SerializeField] private float _fadeTime;
    [SerializeField] private float _endValue;
    public UnityEvent OnFadeEnd;

    public void Fade()
    {
        DOTween.Sequence()
        .Append(_imgToFade.DOFade(_endValue, _fadeTime))
        .AppendCallback(() => OnFadeEnd?.Invoke());
    }
}
