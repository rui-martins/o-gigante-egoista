using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectCaller : MonoBehaviour
{
    [SerializeField] private Clickable[] clickableObjs;
    [SerializeField] private SpineAnimEvent[] animEventObjs;

    public void SetInteractivity(bool val)
    {
        if (clickableObjs is null) return;

        foreach (Clickable clickable in clickableObjs)
            clickable.SetInteractivity(val);
    }

    public void PlayAnimLoop(string state)
    {
        if (animEventObjs is null) return;

        foreach (SpineAnimEvent animEvent in animEventObjs)
            animEvent.SetAnimationStateLoop(state);
    }

    public void PlayAnimNoLoop(string state)
    {
        if (animEventObjs is null) return;

        foreach (SpineAnimEvent animEvent in animEventObjs)
            animEvent.SetAnimationStateNoLoop(state);
    }
}
