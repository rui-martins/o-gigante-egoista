using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimationEventHelper : MonoBehaviour
{
    [SerializeField] private ParticleSystem ps;
    [SerializeField] private SwipeDetection swipeDetect;
    public bool swipeUp;
    public bool swipeRight;
    public bool interactable;

    public UnityEvent OnSwipeUp;
    public UnityEvent OnSwipeRight;

    private void Awake()
    {
        swipeDetect.OnSwipeRight += SwipeRight;
        swipeDetect.OnSwipeUp += SwipeUp;
    }

    public void SetInteractable(bool val) => interactable = val;

    public void PlayParticles()
    {
        ps.Play();
    }

    public void StopParticles()
    {
        ps.Stop();
    }

    public void SwipeRight()
    {
        if (!swipeRight || !interactable) return;
        OnSwipeRight?.Invoke();
    }

    public void SwipeUp()
    {
        print("reached here!");
        if (!swipeUp || !interactable) return;
        OnSwipeUp?.Invoke();
    }
}
